#!/bin/bash

if [ ! -d bin ]; then
    mkdir bin
else
    rm -rf bin
    mkdir bin
fi

if WebGrep.jar; then
    rm WebGrep.jar
fi

javac -cp lib/jsoup-1.12.1.jar -d bin src/sujet/*.java

jar cvfm WebGrep.jar Manifest.txt -C bin .

chmod +x WebGrep.jar

read -p  "Entrer le url de la page web: " url
read -p  "Entrer le mot à chercher: " parametre


if [ "${#url}" -gt 0 ] && [ "${#parametre}" -gt 0 ]; then
    echo "Lancement du programme avec les paramètres suivants"
    echo "URL: $url"
    echo "Mot à chercher: $parametre"
    java -jar WebGrep.jar -cT --threads=1000 "$parametre" "$url"
else
    echo "Lancement du programme avec les paramètres par défaut"
    echo "URL: https://fr.wikipedia.org/wiki/Nantes"
    echo "Mot à chercher: Nantes"
    java -jar WebGrep.jar
fi
