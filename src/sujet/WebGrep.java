package sujet;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class WebGrep {

    // Indique si le programme est terminé ou non
    private static volatile boolean finis = false;

    public static void main(String[] args) throws InterruptedException, IOException {

        // Initialisation du programme avec un arguments par défaut avec deux options de traitement
        if (args.length == 0) Tools.initialize("-cT --threads=1000 Nantes https://fr.wikipedia.org/wiki/Nantes");
        else Tools.initialize(args);

        // Déclaration des deux files de priorité
        // urlNonVisite contient les URL non visitées
        // urlVisite contient les URL visitées
        // Lorsqu'une URL est visitée, elle est retirée de urlNonVisite et ajoutée à urlVisite afin qu'elle ne soit pas
        // visitée deux fois
        ConcurrentHashMap<String, Boolean> urlVisite = new ConcurrentHashMap<>();

        // Ajout de l'url de départ dans la file des URL non visitées
        LinkedBlockingQueue<String> urlNonVisite = new LinkedBlockingQueue<>(Tools.startingURL());

        // Création du pool de threads et lancement des threads dans le pool de threads avec un nombre de threads égal
        // au nombre de threads
        ExecutorService executor = Executors.newFixedThreadPool(Tools.numberThreads());

        // Lancement des threads
        for (int i = 0; i < Tools.numberThreads(); i++) {
            executor.execute(() -> {
                // Tant que le programme n'est pas terminé, on continue à parcourir les URL
                while (!finis) {
                    String url = null;
                    try {
                        // On prend la première URL de la file des URL non visitées
                        url = urlNonVisite.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // Si l'URL n'a pas déjà été visitée, on la visite
                    if (urlVisite.putIfAbsent(url, true) == null) {
                        try {
                            // Parsage de l'URL
                            Tools.ParsedPage p = Tools.parsePage(url);
                            if (!p.matches().isEmpty()) {
                                System.out.println(p.address());
                            }
                            for (String s : p.hrefs()) {
                                // Si l'URL n'a pas déjà été visitée et n'est pas dans la file des URL non visitées,
                                // on l'ajoute à la file des URL non visitées
                                if (!urlVisite.contains(s) && !urlNonVisite.contains(s)) {
                                    urlNonVisite.put(s);
                                }
                            }
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    // Si la file des URL non visitées est vide, on arrête le programme
                    if (urlNonVisite.isEmpty()) {
                        finis = true;
                    }
                }
            });
        }

        // On attend que tous les threads soient terminés
        executor.shutdown();
    }
}